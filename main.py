from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry_home/datasheets"



I = input("Drücke h für 'setup_heat_capacity' oder n für 'setup_newton'") 		#Input für Eingabe auf Tastatur

while True:
    if "h" == I:
        i = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"   	#Tasten für Versuchsstart definieren
        data_path = "/home/pi/calorimetry_home/H5"
        break
    
    if "n" == I:
        i = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
        data_path = "/home/pi/calorimetry_home/H5Newton"
        break

metadata = m_json.get_metadata_from_setup(i)			#Metadaten auslesen
    
metadata = m_json.add_temperature_sensor_serials(path, metadata) 		#Seriennummern zum Sensor hinzufügen
    
data = m_pck.get_meas_data_calorimetry(metadata) 		#Messdaten auslesen
    
m_pck.logging_calorimetry(data, metadata, data_path, path) 		#h5-file erstellen
    
m_json.archiv_json(path, i, data_path) 		#json-Dateien in h5-ordner speichern 
    
