{'alle': {'name': ['group_info',
                   'raspberry_pi',
                   'temperature_environment',
                   'probe_description',
                   'probe_description',
                   'temperature_cup'],
          'values': ['1ee80a55-c676-6588-abea-3fd201035103',
                     '1ee80a44-6c21-6502-b02c-00678e9f1f40',
                     '1ee809ee-496a-6c68-b1ac-24925961eec5',
                     '1ee80a58-b31c-61d9-bc14-1054e242c842',
                     '1ee80a59-6cfa-6fd4-afab-e5a90fc8efa9',
                     '1ee809f4-469b-61f3-b8b0-e35af6ea279e']},
 'group_info': {'name': ['group_info'],
                'values': ['1ee80a55-c676-6588-abea-3fd201035103']},
 'instrument': {'name': ['raspberry_pi'],
                'values': ['1ee80a44-6c21-6502-b02c-00678e9f1f40']},
 'probecapacity': {'name': ['probe_description'],
                   'values': ['1ee80a58-b31c-61d9-bc14-1054e242c842']},
 'probenewton': {'name': ['probe_description'],
                 'values': ['1ee80a59-6cfa-6fd4-afab-e5a90fc8efa9']},
 'sensor': {'name': ['temperature_environment', 'temperature_cup'],
            'serial': ['3ce104574e20', '3ce1e3803c3b'],
            'values': ['1ee809ee-496a-6c68-b1ac-24925961eec5',
                       '1ee809f4-469b-61f3-b8b0-e35af6ea279e']}}




{
    "1ee809ee-496a-6c68-b1ac-24925961eec5": [
        [
            18.25,
            18.25,
            18.25
        ],
        [
            1.4166314601898193,
            3.025454521179199,
            4.626900672912598
        ]
    ],
    "1ee809f4-469b-61f3-b8b0-e35af6ea279e": [
        [
            18.875,
            18.875
        ],
        [
            2.224724054336548,
            3.8251733779907227
        ]
    ]
}


{'1ee809ee-496a-6c68-b1ac-24925961eec5': [[20.125, 20.125], [0.852839469909668, 2.458245038986206]], '1ee809f4-469b-61f3-b8b0-e35af6ea279e': [[20.625, 20.6875], [1.6577391624450684, 3.2897088527679443]]}